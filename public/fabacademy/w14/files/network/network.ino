#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>
 
const int pinCE = 9;
const int pinCSN = 10;
RF24 radio(pinCE, pinCSN);
 
// Single radio pipe address for the 2 nodes to communicate.
const uint64_t pipe = 4747;
 
char data[16]="La luz es:" ;
 
void setup(void)
{
   Serial.begin(9600);
   radio.begin();
   radio.openWritingPipe(pipe);
}
 
void loop(void)
{
   
   int ldr = analogRead(0);
   Serial.println(ldr);

    char b[4];
    String str;
    str=String(ldr);
    str.toCharArray(b,4);
   
   //radio.write(data, sizeof data);
   radio.write(b, sizeof (b));
   delay(1000);
}
