//libraries
#include <avr/sleep.h>
#include <avr/wdt.h>
#include "tinysnore.h" // Include TinySnore Library

//variables
int pTpin = 3; //photoTransistor
int aPpin = 1; //airPump

int pTvalue = 0; //
int pTthreshold = 900;



//setup
void setup()
{
  pinMode(aPpin, OUTPUT); //AIR PUMP PIN
  digitalWrite(aPpin, LOW);
  pinMode(pTpin, INPUT);  //PHOTORESISTOR INPUT PIN
 
}

//LOOP
void loop()
{
  pTvalue = analogRead(pTpin);
  if (pTvalue >= pTthreshold)
  {
    digitalWrite(aPpin, HIGH);
  } 
  else { digitalWrite(aPpin, LOW); }
//  snore(5000); // Deep sleeps for 5 seconds, (low power) then resumes from here

}
